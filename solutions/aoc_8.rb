class Aoc8
  def initialize(input)
    @input = input.split("\n")
    @grid = Grid.new(width: 50, height: 6)
  end

  def solve
    @input.each do |line|
      puts line
      commands = line.split(" ")

      case commands.first
      when "rect"
        width, height = commands[1].split("x")
        @grid.rect(width: width.to_i, height: height.to_i)
      when "rotate"
        type = commands[1]
        idx = commands[2].split("=").last.to_i
        by = commands.last.to_i

        case type
        when "row"
          @grid.rotate_row(row: idx, by: by)
        when "column"
          @grid.rotate_column(col: idx, by: by)
        end
      end
        puts @grid.display
        puts "\n"
    end

    puts @grid.count_pixels
    # require 'pry-byebug'; binding.pry
    # puts @grid.display
    # puts "\n"
    # puts "*****************************"
    # puts @grid.display
    # @grid.rect(w: 3, h: 2)
    # @grid.rotate_column(col: 0, by: 1)
    # puts @grid.display
    # puts "\n"
    # puts "*****************************"
  end
end