class Aoc2
  VALID_DIRECTIONS = { 1   => {"D" => 3},
                       2   => {"R" => 3, "D" => 6},
                       3   => {"R" => 4, "L" => 2, "U" => 1, "D" => 7},
                       4   => {"L" => 3, "D" => 8},
                       5   => {"R" => 6},
                       6   => {"R" => 7, "L" => 5, "U" => 2, "D" => "A"},
                       7   => {"R" => 8, "L" => 6, "U" => 3, "D" => "B"},
                       8   => {"R" => 9, "L" => 7, "U" => 4, "D" => "C"},
                       9   => {"L" => 8},
                       "A" => {"U" => 6, "R" => "B"},
                       "B" => {"R" => "C", "L" => "A", "U" => 7, "D" => "D"},
                       "C" => {"L" => "B", "U" => 8},
                       "D" => {"U" => "B"} }
  MOVE_MAP = { "U" => -3,
               "D" =>  3,
               "L" => -1,
               "R" =>  1 }

  def initialize(file)
    @input = file.split("\n")
    # @start = 5
  end

  def solve
    current = 5
    digits = []

    poop = @input.map(&:chars)

    poop.each do |inst_set|
      inst_set.each do |instruction|
        current = apply(current, instruction)
      end
      digits << current
    end
    puts digits.join
  end

  def apply(current, instruction)
    if valid_instruction?(current, instruction)
      VALID_DIRECTIONS[current][instruction]
    else
      current
    end
  end

  def valid_instruction?(current, instruction)
    VALID_DIRECTIONS[current].keys.include?(instruction)
  end
end