# Each room consists of an encrypted name (lowercase letters separated by dashes)
# followed by a dash, a sector ID, and a checksum in square brackets.

# A room is real (not a decoy) if the checksum is the five most common letters in the encrypted name,
# in order, with ties broken by alphabetization. For example:

# aaaaa-bbb-z-y-x-123[abxyz] is a real room because the most common letters are a (5), b (3),
# and then a tie between x, y, and z, which are listed alphabetically.
# a-b-c-d-e-f-g-h-987[abcde] is a real room because although the letters are all tied (1 of each),
# the first five are listed alphabetically.
# not-a-real-room-404[oarel] is a real room.
# totally-real-room-200[decoy] is not.
# Of the real rooms from the list above, the sum of their sector IDs is 1514.

# What is the sum of the sector IDs of the real rooms?

class Aoc4
  SECTOR_ID_REG = /[0-9]+/
  ROOM_NAME_REG = /([a-z]+\-)+/
  CHECKSUM_REG = /\[+[a-z]+\]/
  CHECKSUM_LENGTH = 5

  def initialize(file)
    @input = file.split("\n")
  end

  def solve
    now = Time.now
    puts now
    sector_id_sum = 0
    room_data = @input.each_with_object([]) do |enc_room_name, collector|
      rn = room_name(enc_room_name)
      si = sector_id(enc_room_name)
      cs = checksum(enc_room_name)
      lf = collate_letter_frequencies_in(room_name(enc_room_name))
      collector << RoomData.new(room_name: rn,sector_id: si,checksum: cs,letter_frequencies: lf)
    end

    room_data.each do |room|
      if is_real?(room)
        sector_id_sum += room.sector_id
        puts decypher_room_name(room)
      end
    end

    puts sector_id_sum
  end

  def collate_letter_frequencies_in(room_name)
    room_name = room_name.gsub('-', '')

    room_name.chars.each_with_object(Hash.new(0)) do |letter, letter_counts|
      letter_counts[letter] += 1
    end.sort_by { |_key, value| value }.reverse
  end

  def is_real?(room)
    expected_checksum = generate_checksum(room.letter_frequencies)

    expected_checksum == room.checksum
  end

  def decypher_room_name(room)
    decrypted_name = []

    room.raw_room_name.chars.each do |letter|
      if letter == "-"
        decrypted_name << " "
        next
      end
      aplh = ("a".."z").to_a

      until aplh.first == letter
        aplh.rotate!
      end

      room.sector_id.times do
        aplh.rotate!
      end

      decrypted_name << aplh.first
    end
    "#{decrypted_name[0, (decrypted_name.size - 1)].join} :: #{room.sector_id}"
  end

  def generate_checksum(letter_frequencies)
    ordered = letter_frequencies.sort_by { |x, y| [ -Integer(y), x ] }
    ordered.map(&:first).first(5).join
  end

  def room_name(encrypted_string)
    ROOM_NAME_REG.match(encrypted_string).to_a.first
  end

  def sector_id(encrypted_string)
    SECTOR_ID_REG.match(encrypted_string).to_a.first.to_i
  end

  def checksum(encrypted_string)
    CHECKSUM_REG.match(encrypted_string).to_a.first
  end
end

class RoomData
  attr_reader :room_name, :raw_room_name, :sector_id, :checksum, :letter_frequencies

  def initialize(room_name:, checksum:, sector_id:, letter_frequencies:)
    @raw_room_name = room_name
    @room_name = room_name.gsub('-', '')
    @sector_id = sector_id
    @checksum = checksum[1,5]
    @letter_frequencies = letter_frequencies
  end
end