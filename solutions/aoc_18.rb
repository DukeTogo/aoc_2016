class Aoc18
  def initialize(input)
    @input = input
    rows = 400000
    @grid = Array.new(rows) { [] }
    populate_rows(input.chars)
    # puts display
  end

  def solve
    # puts display
    (1...@grid.size).each do |x|
      (0...@grid[0].size).each do |y|
        p = Position.new(x: x, y: y)
        left, right, center = p.left, p.right, p.center

        prev_row = collect_other_positions(left, right, center)
        # Its left and center tiles are traps, but its right tile is not.
        if (prev_row[:left].trap? && prev_row[:center].trap? && prev_row[:right].safe?) ||
        # Its center and right tiles are traps, but its left tile is not.
           (prev_row[:left].safe? && prev_row[:center].trap? && prev_row[:right].trap?) ||
        # Only its left tile is a trap.
           (prev_row[:left].trap? && prev_row[:center].safe? && prev_row[:right].safe?) ||
        # Only its right tile is a trap.
           (prev_row[:left].safe? && prev_row[:center].safe? && prev_row[:right].trap?)

          p.assign_type('^')
        else
          p.assign_type('.')
        end

        @grid[x][y] = p
      end

      if @grid[0] == @grid[x]
        require 'pry-byebug'; binding.pry
      end
    end
    # puts display

    counts = @grid.map do |line|
      line.select(&:safe?).count
    end

    puts counts.inject(0, :+)
  end

  def collect_other_positions(left, right, center)
    neighbours = {}

    x, y = left
    neighbours[:left]   = outside_bounds?(x, y) ?  Position.new(x: x, y: y) : @grid[x][y]
    x, y = right
    neighbours[:right]  = outside_bounds?(x, y) ?  Position.new(x: x, y: y) : @grid[x][y]
    x, y = center
    neighbours[:center] = outside_bounds?(x, y) ?  Position.new(x: x, y: y) : @grid[x][y]


   # if neighbours[:right].nil?
   #  require 'pry-byebug'; binding.pry
   #   puts neighbours
   # end
    neighbours
  end

  def populate_rows(input_chars)
    first_row = []
    input_chars.each_with_index do |char, index|
      p = Position.new(x: 0, y: index)
      p.assign_type(char)
      first_row << p
    end

    (@grid.size).times do |i|
      @grid[i] = Array.new(first_row.length, "X")
    end


    @grid[0] = first_row
  end

  def height
    @grid.size
  end

  def width
    @grid[0].size
  end

  def outside_bounds?(x, y)
    !((0...height).include?(x)) || !((0...width).include?(y))
  end

  def display
    @grid.each_with_object("") do |line, output_string|
      output_string << line.join + "\n"
    end
  end
end