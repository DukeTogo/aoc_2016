class Aoc12
  DIGIT = /^[0-9]+$/
  def initialize(input)
    @input = input.split("\n")
    @regs = { "a" => 0,
              "b" => 0,
              "c" => 1,
              "d" => 0 }
    @stack = []
  end

  def solve
    @stack = @input.each_with_object([]) do |instr, stack|
      set = instr.split(" ")

      meth = set.first
      case meth
      when "cpy"
        meth, val, reg = set

        meth = DIGIT.match(val) ? :cpy_val : :cpy_reg
        val = meth == :cpy_val ? val.to_i : val

        stack << [meth, val, reg]
      when "jnz"
        meth, reg, offset = set
        stack << [meth.to_sym, reg, offset.to_i]
      else
        meth, reg = set
        stack << [meth.to_sym, reg]
      end
    end
    running = true
    current = 0

    begin
      while running do
        meth = @stack[current].first
        args = @stack[current][1..-1]

        if meth != :jnz
          self.send(meth, *args)
          current += 1
        else
          reg, offset = args

          if @regs[reg] == 0
            current += 1
          else
            current += offset
            running = false unless current <= @stack.size
          end
        end
      end
    rescue Exception => e
      puts @regs
    end
  end

  def cpy_val(val, reg)
    @regs[reg] = val
  end

  def cpy_reg(regfr, regto)
    @regs[regto] = @regs[regfr]
  end

  def inc(reg)
    @regs[reg] += 1
  end

  def dec(reg)
    @regs[reg] -= 1
  end

  def jnz(reg, offset)
    @regs[reg] == 0 ? 1 : offset
  end
end