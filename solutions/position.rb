class Position
  attr_accessor :x, :y, :wall

  def initialize(x:, y:, is_a_wall: false, type: '.')
   @x = x
   @y = y
   @wall = is_a_wall
   @type = type
  end

  def assign_type(type)
    case type
    when '^'
      @type = '^'
    when '.'
      @type = '.'
    end
  end

  def left
    [x - 1, y - 1]
  end

  def right
    [x - 1, y + 1]
  end

  def center
    [x - 1, y]
  end

  def trap?
    @type == '^'
  end

  def safe?
    @type == '.'
  end

  def show_type
    @type
  end

  def to_s
    # return "8" if @x == 1 && @y == 1
    @type
  end

  def ==(other)
    other.x == @x && other.y == @y
  end
end