class Aoc3
  def initialize(file)
    @input = file.split("\n")
  end

  def solve
    @input = @input.map(&:split)
    flattened = []

    3.times do |i|
      @input.each do |tuple|
        flattened << tuple[i].to_i
      end
    end

    validities = []

    flattened.each_slice(3) do |a, b, c|
      validities << is_valid_triangle?(a, b, c)
    end

    puts validities.select {|a| a}.count
  end

  def part_1
    tris = @input.map do |sides|
      sides = sides.split(" ")
      a = sides.shift.to_i
      b = sides.shift.to_i
      c = sides.shift.to_i

      is_valid_triangle?(a, b, c)
    end

    puts tris.select {|a| a}.count
  end

  def is_valid_triangle?(a, b, d)
    (a + b > d) && (a + d > b) && (b + d > a)
  end
end