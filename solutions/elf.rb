class Elf
  attr_reader :has_presents

  def initialize(pos)
    @pos = pos
    @has_presents = true
  end

  def no_pressies?
    !@has_presents
  end

  def take_pressies!
    @has_presents = false
  end
end