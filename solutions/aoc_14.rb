require 'digest'

class Aoc14
  TRIPLET = /([0-9,a-z,A-Z])\1\1/
  QUINTET = /([0-9,a-z,A-Z])\1\1\1\1/

  def initialize(input)
    @input = "abc"
    # @input = "ahsbgdzn"
    @keys = []
    @max_keys = 64
    @hasher = Digest::MD5.new
  end

  def solve
    index = 0
    while @keys.size < @max_keys do
      haash = @hasher.hexdigest "#{@input}#{index}"
      2016.times do
        haash = @hasher.hexdigest haash
      end

      match = TRIPLET.match(haash)

      if match
        key_char = match[0].chars.uniq
        check_from = index + 1

        ((check_from)...(check_from + 999)).each do |i|
          next_haash = @hasher.hexdigest "#{@input}#{i}"
          next_match = QUINTET.match(next_haash)

          if next_match
            if next_match[0].chars.uniq == key_char
              puts "Index: #{@input}#{index} for #{haash} : matched #{match} with #{key_char} and #{next_haash} matched #{next_match}"
              @keys << haash
              break
            end
          end
        end
      end

      index += 1
    end

    puts "INDEX : #{index}"
  end
end