class Aoc13
  def initialize(fav_number)
    @fav_number = fav_number.to_i
    @grid       = Array.new(100) {[]}
    @current     = Position.new(1, 1)
    @goal        = Position.new(31, 39)
  end

  def solve
    (0...100).each do |x|
      (0...100).each do |y|
        @grid[x][y] = Position.new(x, y, wall_or_space(x, y))
      end
    end

    until @current == @goal
      @current = next_position
    end
  end

  def next_position
    available_positions = positions_from(@current).reject!(&:wall)
    available_positions.sort {|pos| distance_between(pos, @goal)}.reverse.first
  end

  def positions_from(current)
    neighbours = [[-1, 0], [0, -1], [0, 1], [1, 0]]

    neighbours.each_with_object([]) do |coords, positions|
      x, y = coords
      positions << @grid[current.x + x][current.y + y]
    end.compact
  end

  def wall_or_space(x, y)
    coord_product = x*x + 3*x + 2*x*y + y + y*y
    coord_product += @fav_number

    coord_product.to_s(2).chars.select {|c| c == "1" }.count % 2 != 0
  end

  def distance_between(from, goal)
    (from.x - goal.x).abs + (from.y - goal.y).abs
  end

  def display
    @grid.each do |line|
      puts line.join
    end
  end
end