class Aoc1
  attr_reader :directions, :origin

  DIR_MAP = { "N" => { "R" => "E", "L" => "W" },
              "S" => { "R" => "W", "L" => "E" },
              "E" => { "R" => "S", "L" => "N" },
              "W" => { "R" => "N", "L" => "S" } }

  VERTICAL   = { "N" => 1, "S" => -1 }
  HORIZONTAL = { "E" => 1, "W" => -1 }

  def initialize(file)
    @directions = file.split(',')
    @facing = "N"
    @origin = Point.new(0,0)
    @current = Point.new(0,0)
    @visited_locales= [@origin]
  end

  def solve
    directions.each do |pair|
      pair = pair.strip
      direction, steps = pair.chars.first, pair.chars[1..-1].join
      move(direction, steps.to_i)
    end

    visits = collate_visit_frequencies
    first_double_visit = visits.detect {|k,v| v == 2}

    puts first_double_visit.first.join(' : ')
    puts "DISTANCE TO FIRST DOUBLE #{taxi_distance_between(@origin, Point.new(first_double_visit.first[0], first_double_visit.first[1]))}"
    puts "DISTANCE #{taxi_distance_between(@origin, @current)}"
  end

  private

  def move(direction, steps)
    @facing = next_direction(direction)

      steps.times do
        if VERTICAL.keys.include?(@facing)
          @current.x += VERTICAL[@facing]
        else
          @current.y += HORIZONTAL[@facing]
        end
          @visited_locales << [@current.x, @current.y]
      end
  end

  def next_direction(direction)
    DIR_MAP[@facing][direction]
  end

  def taxi_distance_between(origin, destination)
    (origin.x - destination.x).abs + (origin.y - destination.y).abs
  end

  def collate_visit_frequencies
    @visited_locales.each_with_object(Hash.new(0)) do |location, collector|
      collector[location] += 1
    end
  end

  def visited_before?
    @visited_locales.include?(@current)
  end
end
