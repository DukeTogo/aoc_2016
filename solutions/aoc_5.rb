require 'digest'

class Aoc5
  DIGIT_REG = /\d/

  def initialize(input)
    @input = "reyedfim"
    @hasher = Digest::MD5.new
  end

  def solve
    now = Time.now
    puts now
    passcode = []
    idx = 0
    until passcode.compact.size == 8
      haash = @hasher.hexdigest "#{@input}#{idx}"

      if first_five_are_zeroes?(haash)
        position, character = find_next_character(haash)
        passcode[position.to_i] ||= character if valid_index?(position)
      end
      idx += 1
    end
    puts "#{Time.now - now} seconds"
    puts "PASSCODE : #{passcode.join}"
  end

  def valid_index?(position)
    if DIGIT_REG.match(position)
      if (0..7).include?(position.to_i)
        true
      end
    else
      false
    end
  end

  def find_next_character(haash)
    [haash[5], haash[6]]
  end

  def first_five_are_zeroes?(haash)
    haash.chars.first(5) == ["0", "0", "0", "0", "0"]
  end
end