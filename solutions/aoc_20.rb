class Aoc20
  def initialize(input)
    @input = input.split("\n")
  end

  def solve
    ranges = @input.each_with_object([]) do |range, ranges|
      # min, max =  range.split("-").each.map(&:to_i)
      ranges << range.split("-").each.map(&:to_i)
      # ranges << (min...max)
    end

    ranges.sort!
    ranges = ranges.map do |range|
      min, max = range
      (min..max)
    end
    idx = 0

    while (idx < ranges.length - 2) do
      pair = ranges[idx..idx + 1]
      require 'pry-byebug'; binding.pry
      if pair.each.map(&:to_a).flatten.uniq.length == (pair[1].max + 1)
        idx += 1
        next
      end

      puts range
      require 'pry-byebug'; binding.pry
      idx += 1
    end
  end
end