class Address
  REG = /\[[a-z, A-Z, 0-9]*\]/

  def initialize(line)
    @line = line
    @hypernet_seqs = []
    @isp_fragments = []
    process(line)
  end

  def process(line)
    next_part = line.dup
    @hypernet_seqs = line.scan(REG)

    @hypernet_seqs.each do |hp|
      if hp == @hypernet_seqs.last
        next_part.split(hp).each do |isp_fragment|
          @isp_fragments << isp_fragment
        end
      else
        isp_fragment = next_part.split(hp)
        @isp_fragments << isp_fragment.first

        next_part.gsub!(isp_fragment.first, "").gsub!(hp, "")
      end
    end
  end

  def supports_tls?
    ip_has_abba? && no_abba_in_hypernet?
  end

  def supports_ssl?
    supernet_abas = collect_abas_from_supernet
    hypernet_babs = collect_abas_from_hypernet
    bab_aba_match?(supernet_abas, hypernet_babs).any?
  end

  def collect_abas_from_supernet
    @isp_fragments.each_with_object([]) do |fragment, collector|
      collector.concat(find_babs(fragment))
    end.reject {|f| f.size == 0}
  end

  def collect_abas_from_hypernet
    @hypernet_seqs.each_with_object([]) do |fragment, collector|
      fragment = fragment[1...-1]
      collector.concat(find_babs(fragment))
    end.reject {|f| f.size == 0}
  end

  def find_babs(fragment)
    three_letter_slices = []
    letters = fragment.chars
    while letters.size >= 3 do
      three_letter_slices << letters[0, 3]
      letters.shift
    end
    three_letter_slices.select { |slice| is_bab?(slice)}
  end

  def is_bab?(slice)
    slice.uniq.count == 2 &&
    slice[0] == slice[2] &&
    slice[1] != slice[2] &&
    slice[0] != slice[1]
  end

  def bab_aba_match?(supernet_abas, hypernet_babs)
    supernet_abas.map do |bab|
      puts bab
      bab_for_test = invert(bab)
      hypernet_babs.include?(bab_for_test)
    end
  end

  def invert(aba)
    [aba[1], aba[0], aba[1]]
  end

  def ip_has_abba?
    @isp_fragments.each_with_object([]) do |isp_fragment, collector|
      collector << contains_anagram?(isp_fragment)
    end.any?
  end

  def no_abba_in_hypernet?
    results = @hypernet_seqs.each_with_object([]) do |hypernet, collector|
      hypernet = hypernet[1...-1]
      collector << contains_anagram?(hypernet)
    end
    results.none?
  end

  def contains_anagram?(line)
    results = []
    letters = line.chars
    while letters.size >= 4
      if is_anagram?(letters[0, 4])
        results << true
      end

      letters.shift
    end
    results.any?
  end


  def is_anagram?(slice)
    slice.uniq.count == 2 &&
    slice[0] == slice[-1] &&
    slice[1] == slice[2]
  end
end