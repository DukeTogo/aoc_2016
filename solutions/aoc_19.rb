class Aoc19
  def initialize(args)
    @input = 3005290
    @elves = []
  end

  def solve
    @input.times { |i| @elves << Elf.new(i + 1)}

    limit = @input

    while @elves.size != 1 do
      @elves.each_with_index do |elf, index|
        next if elf.no_pressies?
        if index == @input - 1
          # require 'pry-byebug'; binding.pry
        end
        left = index + 1 == limit ? 0 : index + 1
        # puts left
        @elves[left].take_pressies! 
      end

      @elves.reject! { | elf| elf.no_pressies? }
      limit = @elves.size
    end

    puts @elves
  end
end