require 'strscan'

class Aoc9
  def initialize(input)
    @input = input
    @output = ""
  end

  def solve

    scanner = StringScanner.new(@input)
    until scanner.eos?
      char  = scanner.get_byte
      case char
      when "("
        instr = ""
        # require 'pry-byebug'; binding.pry

        while scanner.peek(1) != ")" do
          instr << scanner.get_byte
        end
        # discard the closing brace of the instruction set
        scanner.get_byte

        # grab the num of bytes to repeat and how many times to repeat them
        number_bytes, repeat = instr.split("x").map(&:to_i)
        scan_and_repeat(scanner, number_bytes, repeat)


        puts char
      else
        @output << char
      end
    end
    puts @output
    puts @output.size
    @output.size
  end

  def solve_two

    scanner = StringScanner.new(@input)
    until scanner.eos?
      char  = scanner.get_byte
      case char
      when "("
        instr = ""
        # require 'pry-byebug'; binding.pry

        while scanner.peek(1) != ")" do
          instr << scanner.get_byte
        end
        # discard the closing brace of the instruction set
        scanner.get_byte

        # grab the num of bytes to repeat and how many times to repeat them
        number_bytes, repeat = instr.split("x").map(&:to_i)
        scan_and_repeat(scanner, number_bytes, repeat)


        puts char
      else
        @output << char
      end
    end
    puts @output
    puts @output.size
    @output.size
  end
  def scan_and_repeat(scanner, number_bytes, repeat)
    section = ""
    number_bytes.times do
      section << scanner.get_byte
    end

    repeat.times do
      @output << section
    end
    # require 'pry-byebug'; binding.pry
  end
end