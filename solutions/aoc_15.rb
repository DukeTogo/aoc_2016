class Aoc15
  def initialize(input)
    data = [[13, 1], [19, 10], [3, 2], [7, 1], [5, 3], [17, 5], [11, 0]]

    @discs = data.each_with_object([]) do |pair, discs|
      number_positions, starting_position = pair
      discs << Disc.new(number_positions:number_positions, starting_position: starting_position)
    end
  end

  def solve
    have_capsule = false
    time = 0
    @discs.map(&:tick)
    puts @discs.map(&:to_s)
    until check_positions.all?
      @discs.map(&:tick)
      # puts "*******************************"
      # puts @discs.map(&:to_s)
      # puts "*******************************"
      time += 1
    end
    puts time
  end

  def check_positions
    return [false] if @discs.first.position != 0
    temp_time = 0

    result = @discs.each_with_object([]) do |disc, results|
      results << (disc.position_at_plus(temp_time) == 0)
      return results unless results.all?
      temp_time += 1
    end
    puts result
    result
  end
end