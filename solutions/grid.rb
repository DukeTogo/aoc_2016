class Grid
  attr_reader :width, :height

  def initialize(width:, height:)
    @width = width
    @height = height
    @pixels = create_pixels
  end

  def create_pixels
    container = []

    @height.times do
      line = []
      @width.times do
        line << "."
      end
      container << line
    end

    container
  end

  def display
    @pixels.each_with_object("") do |line, output_string|
      output_string << line.join + "\n"
    end
  end

  def rect(width:, height:)
    # return unless inbounds(width: w, height: h)
    (0...height).each do |i|
      (0...width).each do |j|
        @pixels[i][j] = "X"
      end
    end
  end

  def inbounds(width:, height:)
    @width < width && @height < height
  end

  def rotate_column(col:, by:)
    transposed = @pixels.transpose

    # by.times do
      transposed[col].rotate!(-by)
    # end

    @pixels = transposed.transpose
  end

  def rotate_row(row:, by:)
    # by.times do
      @pixels[row].rotate!(-by)
    # end
  end

  def count_pixels
    @pixels.flatten.select { |pix| pix=="X" }.count
  end
end