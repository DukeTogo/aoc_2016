class Aoc21
  def initialize(input)
    @input = input.split("\n")
    @input = input.split("\n")
    @password = "abcdefgh".chars
    @stack = []
  end


  def solve
    @input.each do |instr|
      set = instr.split(" ")

      meth = set.first
      puts "#{set}"
      puts @password.join

      case meth
      when "swap"
        case set[1]
        when "position"
          # swap position 4 with position 0 swaps the first and last letters,
          # producing the input for the next step, ebcda.
          _,_, a, _, _, b = set
          a = a.to_i
          b = b.to_i

          a_char = @password[a]
          b_char = @password[b]

          @password[a] = b_char
          @password[b] = a_char
        when "letter"
          _,_, a, _, _, b = set

          idx_a = @password.index(a)
          idx_b = @password.index(b)

          @password[idx_a] = b
          @password[idx_b] = a
          # swap letter d with letter b swaps the positions of d and b: edcba.

        end
        # ////////
      when "reverse"
        _, _, from, _, to = set

        from = from.to_i
        to = to.to_i

        @password[from..to] =  @password[from..to].reverse
        # reverse positions 0 through 4 causes the entire string to be reversed, producing abcde.
      when "rotate"
        case set[1]
        when "based"
          # rotate based on position of letter b finds the index of letter b (1), then rotates the string right once plus a number of times equal to that index (2): ecabd.
          # rotate based on position of letter d finds the index of letter d (4), then rotates the string right once, plus a number of times equal to that index, plus an additional time because the index was at least 4, for a total of 6 right rotations: decab.
          _, _, _, _, _, _, letter = set

          letter_index = @password.index(letter)
          # require 'pry-byebug'; binding.pry
          rotations = letter_index >= 4 ? letter_index += 2 : letter_index += 1
          rotations = rotations * (-1)
          puts "Rotatiosn #{rotations}"
          @password.rotate!(rotations)
        else
          # rotate left 1 step shifts all letters left one position, causing the first letter to wrap to the end of the string: bcdea.
          _, dir, num_rotations, _ = set

          num_rotations = num_rotations.to_i
          rot_dir = dir == "left" ? 1 : -1

          @password.rotate!(num_rotations * rot_dir)
        end
      when "move"
        _, _, index, _, _, to = set
        index = index.to_i
        to = to.to_i
        # require 'pry-byebug'; binding.pry
        letter = @password.delete_at(index)

        @password.insert(to, letter)
        # move position 1 to position 4 removes the letter at position 1 (c), then inserts it at position 4 (the end of the string): bdeac.
        # move position 3 to position 0 removes the letter at position 3 (a), then inserts it at position 0 (the front of the string): abdec.
      end
    end
    puts "FINAL: #{@password.join}"
  end

# swap position 4 with position 0 swaps the first and last letters, producing the input for the next step, ebcda.
# swap letter d with letter b swaps the positions of d and b: edcba.
# reverse positions 0 through 4 causes the entire string to be reversed, producing abcde.
# rotate left 1 step shifts all letters left one position, causing the first letter to wrap to the end of the string: bcdea.
# move position 1 to position 4 removes the letter at position 1 (c), then inserts it at position 4 (the end of the string): bdeac.
# move position 3 to position 0 removes the letter at position 3 (a), then inserts it at position 0 (the front of the string): abdec.
# rotate based on position of letter b finds the index of letter b (1), then rotates the string right once plus a number of times equal to that index (2): ecabd.
# rotate based on position of letter d finds the index of letter d (4), then rotates the string right once, plus a number of times equal to that index, plus an additional time because the index was at least 4, for a total of 6 right rotations: decab.

end