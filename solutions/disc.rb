class Disc
  def initialize(number_positions:, starting_position:)
    @positions = (0...number_positions).to_a

    until @positions.first == starting_position
      tick
    end
  end

  def next(disc)
    @next = disc
  end

  def position_at_plus(time)
    @positions.rotate(time).first
  end

  def position
    @positions.first
  end

  def tick
    @positions.rotate!
  end


  def to_s
    @positions.join(' : ')
  end
end