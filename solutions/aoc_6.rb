class Aoc6
  def initialize(input)
    @input = input.split("\n")
  end

  def solve
    puts "SOlving 6"
    lines_array = []

    @input.map do |line|
      lines_array << line.chars
    end

    lines_array = lines_array.transpose
    counts = {}

    lines_array.each_with_index do |row, index|
      row.each do|letter|
        counts[index] ||= {}
        counts[index][letter] ||= 0

        counts[index][letter] += 1
      end
    end
    output = ""
    counts.keys.each do |key|
      sorted = counts[key].sort_by { |k, v| v }
      output << sorted.first[0]
    end

    puts output
  end
end