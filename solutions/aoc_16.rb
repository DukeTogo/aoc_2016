class Aoc16
  def initialize(args)
    @input = "01000100010010111"
    @disc_space = 35651584
  end

  def solve
    data = @input.dup
    while data.length < @disc_space
      b = data.reverse.dup

      b = b.chars.map { |char| char =='1' ? '0' : '1'  }.join
      data << "0#{b}"
    end

    # Truncate output to exactly disc space
    data = data[0...@disc_space]

    checksum = generate_checksum(data)

    while checksum.length % 2 == 0
      checksum = generate_checksum(checksum)
    end
    puts "CHECKSUM: #{checksum}"
  end

  def generate_checksum(data)
    checksum = ""

    data.chars.each_slice(2) do |pair|
      a, b = pair.map(&:to_i)
      c = a == b ? "1" : "0"
      checksum << c
    end

    checksum
  end
end