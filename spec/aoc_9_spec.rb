require "./setup"
describe Aoc9 do
  let(:solver) { Aoc9.new(input) }
  describe '#solve' do
    context 'the input string is ADVENT' do
      let(:input) { "ADVENT" }

      it 'outputs 6' do
        expect(solver.solve).to eq 6
      end
    end
  end
end