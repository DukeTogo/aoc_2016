require "./setup"

describe Aoc2 do
  let(:solver) { Aoc4.new("") }
  let(:room_enc_name) { "aaaaa-bbb-z-y-x-123[abxyz]" }

  describe '#solve' do

  end

  describe '#collate_letter_frequencies_in' do
    context 'room name is ' do
      let(:room_name)          { "bbb-aaaaa-z-y-x-" }
      let(:expected_result)    { {"a" => 5, "b" => 3, "z" => 1, "y" => 1, "x" => 1} }
      let(:expected_key_order) { ["a", "b", "z", "y", "x"] }

      it 'returns a grouped hash of letters ordered by frequency' do
        expect(solver.collate_letter_frequencies_in(room_name)).to eq expected_result
        expect(expected_result.keys).to eq expected_key_order
      end
    end
  end

  describe '#room_name' do
    let(:room_name) { "aaaaa-bbb-z-y-x-" }

    it 'return the correct room_name' do
      expect(solver.room_name(room_enc_name)).to eq room_name
    end
  end

  describe '#sector_id' do
    let(:sector_id) { "123" }

    it 'return the correct sector_id' do
      expect(solver.sector_id(room_enc_name)).to eq sector_id
    end
  end

  describe '#checksum' do
    let(:checksum) { "[abxyz]" }

    it 'return the correct checksum' do
      expect(solver.checksum(room_enc_name)).to eq checksum
    end
  end

  describe '#decypher_room_name' do
    let(:enc_room_name) { "qzmt-zixmtkozy-ivhz-343" }
    let(:room)          { RoomData.new(solver.room_name(enc_room_name),
                                       solver.sector_id(enc_room_name),
                                       # solver.checksum(enc_room_name),
                                       solver.collate_letter_frequencies_in(solver.room_name(enc_room_name)))}
    let(:decrypted_name) {"very encrypted name"}

    it 'returns the decrypted name' do
      expect(solver.decypher_room_name(room)).to eq decrypted_name
    end
  end

  describe '#is_real_room' do

    context 'the room name is real' do
      it 'returns true' do
        
      end
    end

    context 'the room name is fake' do
      # totally-real-room-200[decoy]
      it 'returns false' do
      end
    end
  end
end