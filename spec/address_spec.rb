require "./setup"

describe Address do
  describe '#is_anagram' do
    let(:address) { Address.new("") }

    context 'the string is abba' do
      let(:isp) { "abba".chars }

      it 'returns true' do
        expect(address.is_anagram?(isp)).to eq true
      end
    end

    context 'the string is aaaa' do
      let(:isp) { "aaaa".chars }

      it 'returns true' do
        expect(address.is_anagram?(isp)).to eq false
      end
    end
  end

  describe '#supports_tls' do
    let(:address) { Address.new(isp_string) }

    context 'abba outside square brackets' do
      let(:isp_string) { "abba[mnop]qrst" }

      it 'returns true' do
        expect(address.supports_tls?).to eq true
      end
    end

    context 'abba within square brackets with abba outside' do
      let(:isp_string) { "abcd[bddb]xyyx" }

      it 'returns false' do
        expect(address.supports_tls?).to eq false
      end
    end

    context 'invalid abba in isp string' do
      let(:isp_string) { "aaaa[qwer]tyui" }

      it 'returns false' do
        expect(address.supports_tls?).to eq false
      end
    end

    context 'abba in a larger string' do
      let(:isp_string) { "ioxxoj[asdfgh]zxcvbn" }

      it 'returns true' do
        expect(address.supports_tls?).to eq true
      end
    end
  end

  describe '#supports_ssl' do
    let(:address) { Address.new(isp_string) }

    context 'aba[bab]xyz supports SSL (aba outside square brackets with corresponding bab within square brackets)' do
      let(:isp_string) { "aba[bab]xyz" }

      it 'returns true' do
        expect(address.supports_ssl?).to eq true
      end
    end

    context 'xyx[xyx]xyx does not support SSL (xyx, but no corresponding yxy).' do
      let(:isp_string) { "xyx[xyx]xyx" }

      it 'returns false' do
        expect(address.supports_ssl?).to eq false
      end
    end

    context 'aaa[kek]eke supports SSL (eke in supernet with corresponding kek in hypernet; the aaa sequence is not related, because the interior character must be different)' do
      let(:isp_string) { "aaa[kek]eke" }

      it 'returns false' do
        expect(address.supports_ssl?).to eq true
      end
    end
    
    context 'zazbz[bzb]cdb supports SSL (zaz has no corresponding aza, but zbz has a corresponding bzb, even though zaz and zbz overlap).' do
      let(:isp_string) { "zazbz[bzb]cdb" }

      it 'returns true' do
        expect(address.supports_ssl?).to eq true
      end
    end
  end
end