require "./setup"

describe Aoc2 do
  describe '#apply' do
    context 'on 1' do
      let(:current_digit) { 1 }
      let(:solver) { Aoc2.new("")  }


      context 'receives instruction U' do
        it 'ignores the instruction' do
          expect(solver.apply(current_digit, "U")).to eq current_digit
        end
      end

      context 'receives instruction D' do
        it 'applies the instruction and returns 4' do
          expect(solver.apply(current_digit, "D")).to eq 3
        end
      end

      context 'receives instruction L' do
        it 'applies the instruction and returns 4' do
          expect(solver.apply(current_digit, "L")).to eq current_digit
        end
      end

      context 'receives instruction R' do
        it 'applies the instruction and returns 2' do
          expect(solver.apply(current_digit, "R")).to eq current_digit 
        end
      end
    end

    context 'on 2' do
      let(:current_digit) { 2 }
      let(:solver) { Aoc2.new("")  }


      context 'receives instruction U' do
        it 'ignores the instruction' do
          expect(solver.apply(current_digit, "U")).to eq current_digit
        end
      end

      context 'receives instruction D' do
        it 'applies the instruction and returns 5' do
          expect(solver.apply(current_digit, "D")).to eq 6
        end
      end

      context 'receives instruction L' do
        it 'applies the instruction and returns 1' do
          expect(solver.apply(current_digit, "L")).to eq current_digit
        end
      end

      context 'receives instruction R' do
        it 'applies the instruction and returns 3' do
          expect(solver.apply(current_digit, "R")).to eq 3
        end
      end
    end

    context 'on 3' do
      let(:current_digit) { 3 }
      let(:solver) { Aoc2.new("")  }


      context 'receives instruction U' do
        it 'ignores the instruction' do
          expect(solver.apply(current_digit, "U")).to eq 1
        end
      end

      context 'receives instruction D' do
        it 'applies the instruction and returns 5' do
          expect(solver.apply(current_digit, "D")).to eq 7
        end
      end

      context 'receives instruction L' do
        it 'applies the instruction and returns 1' do
          expect(solver.apply(current_digit, "L")).to eq 2
        end
      end

      context 'receives instruction R' do
        it 'applies the instruction and returns 3' do
          expect(solver.apply(current_digit, "R")).to eq 4
        end
      end
    end

    context 'on 4' do
      let(:current_digit) { 4 }
      let(:solver) { Aoc2.new("")  }


      context 'receives instruction U' do
        it 'ignores the instruction' do
          expect(solver.apply(current_digit, "U")).to eq current_digit
        end
      end

      context 'receives instruction D' do
        it 'applies the instruction and returns 5' do
          expect(solver.apply(current_digit, "D")).to eq 8
        end
      end

      context 'receives instruction L' do
        it 'applies the instruction and returns 1' do
          expect(solver.apply(current_digit, "L")).to eq 3
        end
      end

      context 'receives instruction R' do
        it 'applies the instruction and returns 3' do
          expect(solver.apply(current_digit, "R")).to eq current_digit
        end
      end
    end

    context 'on 5' do
      let(:current_digit) { 5 }
      let(:solver) { Aoc2.new("")  }


      context 'receives instruction U' do
        it 'ignores the instruction' do
          expect(solver.apply(current_digit, "U")).to eq current_digit
        end
      end

      context 'receives instruction D' do
        it 'applies the instruction and returns 5' do
          expect(solver.apply(current_digit, "D")).to eq current_digit
        end
      end

      context 'receives instruction L' do
        it 'applies the instruction and returns 1' do
          expect(solver.apply(current_digit, "L")).to eq current_digit
        end
      end

      context 'receives instruction R' do
        it 'applies the instruction and returns 3' do
          expect(solver.apply(current_digit, "R")).to eq 6
        end
      end
    end

    context 'on 6' do
      let(:current_digit) { 6 }
      let(:solver) { Aoc2.new("")  }


      context 'receives instruction U' do
        it 'ignores the instruction' do
          expect(solver.apply(current_digit, "U")).to eq 2
        end
      end

      context 'receives instruction D' do
        it 'applies the instruction and returns 5' do
          expect(solver.apply(current_digit, "D")).to eq "A"
        end
      end

      context 'receives instruction L' do
        it 'applies the instruction and returns 1' do
          expect(solver.apply(current_digit, "L")).to eq 5
        end
      end

      context 'receives instruction R' do
        it 'applies the instruction and returns 3' do
          expect(solver.apply(current_digit, "R")).to eq 7
        end
      end
    end

    context 'on 7' do
      let(:current_digit) { 7 }
      let(:solver) { Aoc2.new("")  }


      context 'receives instruction U' do
        it 'ignores the instruction' do
          expect(solver.apply(current_digit, "U")).to eq 3
        end
      end

      context 'receives instruction D' do
        it 'applies the instruction and returns 5' do
          expect(solver.apply(current_digit, "D")).to eq "B"
        end
      end

      context 'receives instruction L' do
        it 'applies the instruction and returns 1' do
          expect(solver.apply(current_digit, "L")).to eq 6
        end
      end

      context 'receives instruction R' do
        it 'applies the instruction and returns 3' do
          expect(solver.apply(current_digit, "R")).to eq 8
        end
      end
    end

    context 'on A' do
      let(:current_digit) { "A" }
      let(:solver) { Aoc2.new("")  }


      context 'receives instruction U' do
        it 'ignores the instruction' do
          expect(solver.apply(current_digit, "U")).to eq 6
        end
      end

      context 'receives instruction D' do
        it 'applies the instruction and returns 5' do
          expect(solver.apply(current_digit, "D")).to eq current_digit
        end
      end

      context 'receives instruction L' do
        it 'applies the instruction and returns 1' do
          expect(solver.apply(current_digit, "L")).to eq current_digit
        end
      end

      context 'receives instruction R' do
        it 'applies the instruction and returns 3' do
          expect(solver.apply(current_digit, "R")).to eq "B"
        end
      end
    end

    context 'on C' do
      let(:current_digit) { "C" }
      let(:solver) { Aoc2.new("")  }


      context 'receives instruction U' do
        it 'ignores the instruction' do
          expect(solver.apply(current_digit, "U")).to eq 8
        end
      end

      context 'receives instruction D' do
        it 'applies the instruction and returns 5' do
          expect(solver.apply(current_digit, "D")).to eq current_digit
        end
      end

      context 'receives instruction L' do
        it 'applies the instruction and returns 1' do
          expect(solver.apply(current_digit, "L")).to eq "B"
        end
      end

      context 'receives instruction R' do
        it 'applies the instruction and returns 3' do
          expect(solver.apply(current_digit, "R")).to eq current_digit
        end
      end
    end

    context 'on D' do
      let(:current_digit) { "D" }
      let(:solver) { Aoc2.new("")  }


      context 'receives instruction U' do
        it 'ignores the instruction' do
          expect(solver.apply(current_digit, "U")).to eq "B"
        end
      end

      context 'receives instruction D' do
        it 'applies the instruction and returns 5' do
          expect(solver.apply(current_digit, "D")).to eq current_digit
        end
      end

      context 'receives instruction L' do
        it 'applies the instruction and returns 1' do
          expect(solver.apply(current_digit, "L")).to eq current_digit
        end
      end

      context 'receives instruction R' do
        it 'applies the instruction and returns 3' do
          expect(solver.apply(current_digit, "R")).to eq current_digit
        end
      end
    end
  end
end