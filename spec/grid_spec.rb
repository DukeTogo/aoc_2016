require "./setup"

describe Grid do
  let(:width)  { 50 }
  let(:height) { 6 }
  let(:grid)   { Grid.new(width: width, height: height) }

  describe '#initialize' do
    it 'creates an internal pixel representation of the correct size' do
      expect(grid.width).to eq width
      expect(grid.height).to eq height
    end
  end

  describe '#display' do
    # let(:grid_display) { "..................................................\n..................................................\n..................................................\n..................................................\n..................................................\n..................................................\n"}

    let(:grid_display) { [
      ["..........\n"],
      ["..........\n"],
      ["..........\n"],
      ["..........\n"],
      ["..........\n"],
      ["..........\n"],
      ["..........\n"],
      ["..........\n"],
      ["..........\n"],
      ["..........\n"],
      ["..........\n"], ].join }
      
    it 'displays the pixels correctly ' do
      expect(grid.display).to eq grid_display
    end
  end

  describe '#rotate_column' do
    let(:grid_display) { [
      ["..........\n"],
      ["..........\n"],
      ["..........\n"],
      ["..........\n"],
      ["..........\n"],
      ["..........\n"],
      ["..........\n"],
      ["..........\n"],
      ["..........\n"],
      ["..........\n"],
      ["..........\n"], ].join }
    let(:width)  { 10 }
    let(:height) { 10 }

  end
end