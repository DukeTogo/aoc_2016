require_relative 'setup'

class Aoc
  INPUTS_PATH = "inputs/"

  def initialize
    solution_number = ARGV.join('')
    solution = Module.const_get("Aoc#{solution_number}")

    Dir.chdir(INPUTS_PATH)

    input = File.read("aoc_#{solution_number}_input.txt")

    solution.new(input).solve
  end
end

Aoc.new